def max_heapify(arr, n, i):
    # largest var is for selecting the largest amongst all
    largest = i
    left = 2 * i + 1
    right = 2 * i + 2
    # left and right children
    if left < n and arr[left] > arr[largest]:
        largest = left
    if right < n and arr[right] > arr[largest]:
        largest = right
    if largest != i:
        temp = arr[i]
        arr[i] = arr[largest]
        arr[largest] = temp
        # Once we've confirme that variable at index i has been updated, max heapify with index as largest
        max_heapify(arr, n, largest)


def build_max_heap(arr, n):
    for i in range(n//2, -1, -1):
        # from n/2 down to start
        max_heapify(arr, n, i)


arr = [12, 11, 13, 5, 6, 7]
build_max_heap(arr, len(arr))
print(arr)
