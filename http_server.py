from http.server import HTTPServer, BaseHTTPRequestHandler
import cgi
# cgi (common gateway interface), built into python,(invoked by httpserver) to process submitted through
# html <form> or <isindex> element

task_list = ['Task 1', 'Task 2', 'Task 3']
# we're gonna remove self.wfile.write() for now, and instead ouput this tasklist onto the browser window



class requestHANDLER(BaseHTTPRequestHandler):
    # this handler will inherit from the BaseHTTPRequestHandler class
    def do_GET(self):
        if self.path.endswith('/tasklist'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()
            # in order to serve up some content, we'll write to the page using wfile
            # encode() method serves up the content in the form of bytes, coz u can't send a string directly
            # self.wfile.write('Hello!'.encode())
            # self.wfile.write(self.path.encode()) for a path localhost:8000/hello would print out '/hello'
            # also self.wfile.write(self.path[1:].encode()) would chop off the '/' and just give 'hello'

            output = ''
            output += '<html><body>'
            output += '<h1>Task List</h1>'
            output += '<h3><a href="/tasklist/new">Add New Task</a></h3>'
            for task in task_list:
                output += task
                output += '</br>'
            output += '</body></html>'
            self.wfile.write(output.encode())

        if self.path.endswith('/new'):
            self.send_response(200)
            self.send_header('content-type', 'text/html')
            self.end_headers()

            output = ''
            output += '<html><body>'
            output += '<h1>Add a new task</h1>'

            output += '<form method="POST" enctype="multipart/form-data" action="/tasklist/new">'
            output += '<input name="task" type="text" placeholder="Add new task">'
            output += '<input type="submit" value="Add">'
            output += '</form>'
            output += '</body></html>'

            self.wfile.write(output.encode())

    def do_POST(self):
        if self.path.endswith('/new'):
            # ctype for content type and pdict for parameter dictionary
            # ctype will be the same as enctype in the form above
            ctype, pdict = cgi.parse_header(self.headers.get('content-type'))
            pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
            content_len = int(self.headers.get('Content-length'))
            pdict['CONTENT-LENGTH'] = content_len
            if ctype == 'multipart/form-data':
                fields = cgi.parse_multipart(self.rfile, pdict)
                new_task = fields.get('task')
                task_list.append(new_task[0])

            self.send_response(301) # 301 is for redirect request
            self.send_header('content-type', 'text/html')
            self.send_header('Location', '/tasklist')
            self.end_headers()

def main():
    PORT = 8000
    server = HTTPServer(('', PORT), requestHANDLER)
    # instantiating the HTTPServer class, first arg is a tuple containing localhost and PORT num
    # next argument is the HANDLER name, used to handle all the GET requests that the server recieves
    print("Server running on port %s" % PORT)
    server.serve_forever()


if __name__ == '__main__':
    main()
