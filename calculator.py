num = int(input("Enter the first number"))

op = input("Enter the operation you wish to perform, +/-/(/)/*")
print("Press a anytime to stop a recurring operation, c to change operator, q to quit")

while True:
    next_num = input("Enter the next number")
    if next_num == 'a':
        break
    elif next_num == 'q':
        break
    elif next_num == 'c':
        op = input("Enter the operation you wish to perform, +/-/(/)/*")
    else:
        if op == '+':
            num += next_num
        elif op == '-':
            num -= next_num
        elif op == '*':
            num *= next_num
        elif op == '/' :
            if next_num == 0:
                print("Invalid division!")
                num /= next_num
        else:
            print("Please enter valid input!")

print("The result is: {}".format(num))
