class Graph:
    def __init__(self, v):
        self.v = v
        self.adjList = {}

    def add_edge(self, u, v, distance):
        v += "," + str(distance)
        if u in self.adjList:
            self.adjList[u].append(v)
        else:
            self.adjList[u] = [v]

    def print_adjList(self):
        for key, value in self.adjList.items():
            print("In order to go from ", key, end='')
            print(' to :')
            for val in value:
                el = val.split(',')
                print(el[0], " we'll have to travel ", el[1], " kms.")



num_vertices = input("Enter the number of vertices in the graph")
graph = Graph(num_vertices)

while True:
    choice = int(input("1. To add vertices to the graph, 2. to display adjacency list, 3. quit"))
    if choice == 3:
        exit()
    elif choice == 1:
        print("Enter the 2 vertices to add an edge to(note that nodes are numbered from 0 to v-1)")
        node1 = input('Enter the first')
        node2 = input('Enter the second')
        distance = int(input("Enter the distance"))
        graph.add_edge(node1, node2, distance)
    elif choice == 2:
        graph.print_adjList()
    else:
        print("Wrong choice, try again!")
