import math

class Calculator:
    def __init__(self, a):
        self.a = a

    def add(self, num):
        self.a += num

    def mul(self, num):
        self.a *= num

    def div(self, num):
        self.a /= num

    def sub(self, num):
        self.a -= num

    def result(self):
        return self.a

#
# num1 = int(input("Enter first number: "))
#
# calculator_obj = Calculator(num1)
#
# op = input("Enter the operation you wish to perform, +/-/(/)/*")
# while True:
#     next_num = input("Enter the next number")
#     if next_num == 'a':
#         break
#     elif next_num == 'c':
#         op = input("Enter the operation you wish to perform, +/-/(/)/*")
#     else:
#         if op == '+':
#             calculator_obj.add(int(next_num))
#         elif op == '-':
#             calculator_obj.sub(int(next_num))
#         elif op == '*':
#             calculator_obj.mul(int(next_num))
#         elif op == '/' :
#             if next_num == '0':
#                 print("Invalid division!")
#             calculator_obj.div(int(next_num))
#         else:
#             print("Please enter valid input!")
#
# print("The result is: {}".format(calculator_obj.result()))


def calculate(op, calculator_obj, next_num):
    if op == '+':
        calculator_obj.add(int(next_num))
    elif op == '-':
        calculator_obj.sub(int(next_num))
    elif op == '*':
        calculator_obj.mul(int(next_num))
    elif op == '/':
        if next_num == '0':
            print("Invalid division!")
        calculator_obj.div(int(next_num))
    else:
        print("Please enter valid input!")


equation = input("Enter the equation: ")

op_list = []

operand_stack = []
operator_stack = []

for char in equation:
    if char.isdigit():
        op_list.append(char)
    elif char in ['+', '-', '*', '/'] or char in ['(', ')', '{', '}', '[', ']']:
        op_list.append(char)
    else:
        continue

# calculator_obj = Calculator(int(op_list[0]))
# next_num = math.inf

# for index in range(1, len(op_list)):
#     if index % 2 == 0:
#         if not op_list[index].isdigit():
#             print("The equation was invalid")
#             break
#         else:
#             next_num = int(op_list[index])
#             calculate(op_list[index - 1], calculator_obj, next_num)
#     else:
#         if op_list[index] not in ['+', '-', '*', '/']:
#             print("The equation contained invalid operators")
#             break
#
#
# print("The result is: {}".format(calculator_obj.result()))


def is_opening(ch):
    if ch in ['(', '{', '[']:
        return True


def is_closing(ch):
    if ch in [')', '}', ']']:
        return True


def precedence(ch):
    if ch == '+' or ch == '-':
        return 1
    elif ch == '*' or ch == '/':
        return 2


def pop_and_push():
    try:
        op = operator_stack.pop()
        op1 = operand_stack.pop()
        op2 = operand_stack.pop()
        calculator_obj = Calculator(int(op1))
        calculate(op, calculator_obj, op2)
        operand_stack.append(calculator_obj.result())
    except Exception(IndexError):
        return


for char in op_list:
    if char not in ['+', '-', '*', '/']:
        if char.isdigit():
            operand_stack.append(char)
        elif is_opening(char):
            operator_stack.append(char)
        else:
            print("Invalid expression")
            exit(0)
    elif char in ['+', '-', '*', '/']:
        if len(operator_stack) != 0:
            while len(operator_stack) != 0 and not precedence(operator_stack[-1]) < precedence(char):
                pop_and_push()
        operator_stack.append(char)
    elif is_closing(char):
        while not is_opening(char):
            pop_and_push()


while len(operator_stack) != 0:
    pop_and_push()

print("The result is: ", operand_stack[0])
