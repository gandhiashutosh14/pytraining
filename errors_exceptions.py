

try:
    # f = open('Original.txt')
    f = open('corrupt_file.txt')
    if f.name == 'corrupt_file.txt':
        raise Exception
    # var = bad_var
except FileNotFoundError as e:
    print(e)
except Exception as e:
    print(e)
else:
    print(f.read())
    f.close()
finally:
    print("Executing finally!")


####### User defined exception with classes, inheriting from Exception class

class UserDefinedError(Exception):
    def __init__(self, message):
        self.message = message


a = 0

try:
    if a == 0:
        raise UserDefinedError("Division by zero!")
except UserDefinedError as e:
    print(e.message)


# Python code to illustrate
# clean up actions
def divide(x, y):
    try:
        # Floor Division : Gives only Fractional Part as Answer
        result = x // y
    except ZeroDivisionError:
        print("Sorry ! You are dividing by zero ")
    else:
        print("Yeah ! Your answer is :", result)
    finally:
        print("I'm finally clause, always raised !! ")



divide(3, "3")


# predefined cleanup actions
with open('examples.txt', 'r') as f:
    read_data = f.read()
    print(read_data)
f.closed
