class Node:
    def __init__(self, d, n = None):
        self.data = d
        self.next_node = n

    def get_next(self):
        return self.next_node

    def set_next(self, n):
        self.next_node = n

    def get_data(self):
        return self.data

    def set_data(self, d):
        self.data = d


class LinkedList:
    def __init__(self, r=None):
        self.root = r
        self.size = 0

    def get_size(self):
        return self.size

    def add(self, d):
        new_node = Node(d, self.root)
        self.root = new_node
        self.size += 1

    def remove(self, d):
        this_node = self.root
        prev_node = None
        while this_node:
            if this_node.get_data() == d:
                if prev_node:
                    prev_node.set_next(this_node.get_next())
                else:
                    self.root = this_node
                self.size -= 1
                return True
            else:
                prev_node = this_node
                this_node = this_node.get_next()
        return False

    def find(self, d):
        this_node = self.root
        while this_node:
            if this_node.get_data() == d:
                return d
            else:
                this_node = this_node.get_next()
        return None

    def print(self):
        this_node = self.root
        while this_node:
            print(this_node.get_data(), end='\t')
            this_node = this_node.get_next()


myList = LinkedList()

while(True):
    choice = input(" 1 to add, 2 to remove, 3 to find, 4 to print, 5 to exit ")
    if choice == '1':
        data = input("Enter the data to add: ")
        myList.add(data)
    elif choice == '2':
        data = input("Enter the data to remove: ")
        if myList.remove(data):
            print("Data removed")
        else:
            print(data, " could not be removed")
    if choice == '3':
        data = input("Enter the data to find: ")
        if myList.find(data):
            print(data, " found")
        else:
            print(data, " not found")
    if choice == '4':
        myList.print()
    if choice == '5':
        exit()
